import { DetalhesPesquisaComponent } from './detalhes-pesquisa/detalhes-pesquisa.component';
import { PesquisaComponent } from './pesquisa/pesquisa.component';
import { GuardService } from './guard/guard.service';


import { Routes, RouterModule } from '@angular/router';
import { ModuleWithProviders } from '@angular/core';
import { NgModule } from '@angular/core';

import { LoginComponent } from './login/login.component';
import { HomeComponent } from './home/home.component';

export const appRoutes: Routes = [
    { path: 'login', component: LoginComponent },
    { path: 'home', component: HomeComponent , canActivate: [GuardService]},
    { path: 'pesquisa', component: PesquisaComponent , canActivate: [GuardService]},
    { path: 'detalhes-pesquisa/:id', component: DetalhesPesquisaComponent, canActivate: [GuardService] }
];
@NgModule({
    imports: [RouterModule.forRoot(appRoutes)],
    exports: [RouterModule]
})

export class AppRoutingModule {

}