import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DetalhesPesquisaComponent } from './detalhes-pesquisa.component';

describe('DetalhesPesquisaComponent', () => {
  let component: DetalhesPesquisaComponent;
  let fixture: ComponentFixture<DetalhesPesquisaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DetalhesPesquisaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DetalhesPesquisaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
