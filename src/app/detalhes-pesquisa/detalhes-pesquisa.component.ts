import { PesquisaService } from './../pesquisa/pesquisa.service';
import { ActivatedRoute, Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-detalhes-pesquisa',
  templateUrl: './detalhes-pesquisa.component.html',
  styleUrls: ['./detalhes-pesquisa.component.css']
})
export class DetalhesPesquisaComponent implements OnInit {
  id: any = "";
  empresa_detalhes: any = [];
  descricao: any = "";
  nomeEmpresa: any = "";
  constructor(private route: ActivatedRoute, private pesquisa: PesquisaService, private router: Router) { }

  ngOnInit() {
    this.route.params.subscribe(
      (params: any) => {
        this.id = params['id'];
      }
    )
    //this.empresa_detalhes = this.pesquisa.get_detalhes(this.id);
    this.pesquisa.get_detalhes(this.id).subscribe(data => {
      this.empresa_detalhes = data['enterprise'];
      this.descricao = this.empresa_detalhes["description"];
      this.nomeEmpresa = this.empresa_detalhes["enterprise_name"];

    })


  }

}
