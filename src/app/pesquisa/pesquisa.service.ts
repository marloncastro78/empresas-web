import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions } from '@angular/http';
import 'rxjs/add/operator/map';
import { Observable } from 'rxjs/Observable';
import "rxjs/add/operator/catch";
import 'rxjs/add/observable/throw';

@Injectable()
export class PesquisaService {

    headers: Headers;
    options: RequestOptions;
    public login: String = "";
    public senha: String = "";
    public autenticado: Boolean = false;
    constructor(public http: Http) {



        this.headers = new Headers({
            'Content-Type': 'application/json',
            'access-token': localStorage.getItem('token'),
            'client': localStorage.getItem('client'),
            'uid': localStorage.getItem('uid'),
        });
        this.options = new RequestOptions({ headers: this.headers });
    }

    listagem_empresas() {
        let url = "http://54.94.179.135:8090/api/v1/enterprises/";
        // let url = "http://54.94.179.135:8090/api/v1/enterprises?name=SA";

        return this.http.get(url, this.options).map(res => res.json()).catch(this.handleError);
    }

    listagem_nomeTipo(nome: any, tipo: any) {
        let complemento = "";
        if (tipo == "") {
            complemento = "name=" + nome;
        } else {
            complemento = "enterprise_types=" + tipo + "&name=" + nome;
        }
        let url = "http://54.94.179.135:8090/api/v1/enterprises?" + complemento;

        return this.http.get(url, this.options).map(res => res.json()).catch(this.handleError);


    }
    get_detalhes(id) {
        let url = "http://54.94.179.135:8090/api/v1/enterprises/"+id;
        // let url = "http://54.94.179.135:8090/api/v1/enterprises?name=SA";

        return this.http.get(url, this.options).map(res => res.json()).catch(this.handleError);
    }

    handleError(error) {
        console.log(error);
        return Observable.throw(error.json().error || 'SERVER ERROR');
    }

}