import { Router } from '@angular/router';
import { PesquisaService } from './pesquisa.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-pesquisa',
  templateUrl: './pesquisa.component.html',
  styleUrls: ['./pesquisa.component.css']
})
export class PesquisaComponent implements OnInit {
  dados_empresa: any[];
  tipo_select = "";
  nome_busca = "";
  constructor(private pesquisa: PesquisaService, private router: Router) {
  }

  ngOnInit() {
    this.pesquisa.listagem_empresas().subscribe(data => {
      this.dados_empresa = data['enterprises'];
    })
  }

  buscaEmpresa(seletor: any) {
    this.pesquisa.listagem_nomeTipo(this.nome_busca, this.tipo_select).subscribe(data => {
      this.dados_empresa = data['enterprises'];
    })
  }

  mostrarDetalhes(id) {
    this.router.navigate(['/detalhes-pesquisa', id]);
  }


}
