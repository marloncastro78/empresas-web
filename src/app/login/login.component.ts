import { Router } from '@angular/router';
import { LoginService } from './login.service';
import { Component, OnInit } from '@angular/core';
import { Http, Headers } from '@angular/http';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  constructor(private logar: LoginService, private router: Router) { }

  ngOnInit() {
  }
  title = 'app';
  email_text: String = "";
  senha_text: String = "";


  autenticar_user() {
    this.logar.login = this.email_text;
    this.logar.senha = this.senha_text;
    this.logar.web_login().subscribe(data => {
      let headers = (data.headers.toJSON());
      let body = (data.json());
      if (body['success']) {
        alert("Login efetuado com sucesso!!");
        this.logar.autenticado = true;
        localStorage.setItem('token', headers['access-token']);
        localStorage.setItem('uid', headers['uid']);
        localStorage.setItem('client', headers['client']);
        this.router.navigate(['/home']);
      } else {
        alert("Credenciais inválidas!");
      }
    }, err => {
      alert("Credenciais inválidas!");
    }
    );
  }

}
