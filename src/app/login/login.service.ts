import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions } from '@angular/http';
import 'rxjs/add/operator/map';
import { Observable } from 'rxjs/Observable';
import "rxjs/add/operator/catch";
import 'rxjs/add/observable/throw';

@Injectable()
export class LoginService {

    headers: Headers;
    options: RequestOptions;
    public login: String = "";
    public senha: String = "";
    public autenticado: Boolean = false;
    constructor(public http: Http) {



        this.headers = new Headers({
            'Content-Type': 'application/json',
        });
        this.options = new RequestOptions({ headers: this.headers });
    }


    web_login() {
        let url = "http://54.94.179.135:8090/api/v1/users/auth/sign_in";
        let body = {
            "email": this.login,
            "password": this.senha
        }
        return this.http.post(url, body, this.options);

    }

    handleError(error) {
        //console.log(error);
        return Observable.throw(error.json().error || 'SERVER ERROR');
    }

    usuarioEstaAutenticado() {
       // alert(this.autenticado);
        return this.autenticado;
    }
}