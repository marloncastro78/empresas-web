import { PesquisaService } from './pesquisa/pesquisa.service';
import { GuardService } from './guard/guard.service';
import { Http, HttpModule } from '@angular/http';
import { LoginService } from './login/login.service';
import { AppRoutingModule } from './app.routing.module';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { HomeComponent } from './home/home.component';
import { PesquisaComponent } from './pesquisa/pesquisa.component';
import { DetalhesPesquisaComponent } from './detalhes-pesquisa/detalhes-pesquisa.component';




@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    HomeComponent,
    PesquisaComponent,
    DetalhesPesquisaComponent
  ],
  imports: [
    HttpModule,
    BrowserModule,
    FormsModule,
    AppRoutingModule
  ],
  providers: [LoginService, GuardService, PesquisaService],
  bootstrap: [AppComponent]
})
export class AppModule { }
