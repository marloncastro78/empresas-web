import { Component } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  logado: boolean = true;
  token = localStorage.getItem('token');
  constructor(private router: Router) {
    if (this.token != "") {
      this.router.navigate(['/login']);
    }else{
       this.router.navigate(['/home']);
    }

  }




}
