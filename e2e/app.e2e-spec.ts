import { EmpresasWebPage } from './app.po';

describe('empresas-web App', () => {
  let page: EmpresasWebPage;

  beforeEach(() => {
    page = new EmpresasWebPage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to app!');
  });
});
